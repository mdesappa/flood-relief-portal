import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MainApp from './App';
import { default as Firebase, FirebaseContext } from './persistance';

const App = () => (
        <FirebaseContext.Provider value={new Firebase()}>
            <MainApp />
        </FirebaseContext.Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
