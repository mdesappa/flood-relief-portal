
import React from 'react';
import app from 'firebase/app';
import firebase from 'firebase';

const prodConfig = {
    apiKey: "AIzaSyCHx2YP9LHIyIsEgiG48genKEmt48CmVDU",
    authDomain: "relief-3fc54.firebaseapp.com",
    databaseURL: "https://relief-3fc54.firebaseio.com",
    projectId: "relief-3fc54",
    storageBucket: "relief-3fc54.appspot.com",
    messagingSenderId: "61520006102",
    appId: "1:61520006102:web:edb7b3cdea118bfb"
};
  
class Firebase {
    constructor() {
      if (!firebase.apps.length) {
        app.initializeApp(prodConfig);
      }
      this.db = app.database();
    }
    affected_zones = () => this.db.ref('affected_zones');
    emergency_contacts = () => this.db.ref('emergency_contacts');
    donation_box = () => this.db.ref('donation_box');
}
  
export default Firebase;


export const FirebaseContext = React.createContext(null);

export const withFirebase = Component => props => (
    <FirebaseContext.Consumer>
      {firebase => <Component {...props} firebase={firebase} />}
    </FirebaseContext.Consumer>
);