import React, { Component } from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    Slider,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    FormInput,
  } from 'shards-react';  
;
import ReCAPTCHA from 'react-google-recaptcha';
import { withFirebase } from '../persistance';
import { UPI_IMAGE } from './upi_image';

export class DonationBoxComponent extends Component {
    constructor() {
        super();
        this.state = {
            donationValue: 50,
            showModal: false,
            isVerified: false,
          };
        this.handleDonationValueChange = this.handleDonationValueChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.donor_name_ref = React.createRef();
    }
    handleDonationValueChange(newValue) {
        this.setState({ donationValue: newValue });
    }
    toggle() {
        this.setState({ showModal: !this.state.showModal });
    }
    onVerify(isVerified) {
        this.setState({ isVerified })
    }
    render() {
        return(
            <Card>
                <CardHeader>
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <h4>💰Donate</h4>
                    <span>Last donated 20 seconds ago</span>
                </CardHeader> 
                <CardBody className="card_height_fix">
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                <p className="donation_area--desc">
                    💚 Your precious donations could help save lives by providing 
                    <span className="donation_area--desc_item">Clean water</span>
                    <span className="donation_area--desc_item">Food</span>
                    <span className="donation_area--desc_item">Medicines</span> 
                    <span className="donation_area--desc_item">First Aid Kits</span> 
                    in the afflicted zones.
                </p>
                <div>
                    <h5 className="donation_value--text">I'd Like to Donate</h5>
                    <span className="donation_value--rupee">₹{this.state.donationValue}</span>
                </div>
                <div className="center donation_value--slider">
                    <Slider
                        style={{ marginTop: "-5px" }}
                        pips={{ mode: "steps", stepped: true, density: 100 }}
                        onSlide={this.handleDonationValueChange}
                        start={this.state.donationValue}
                        range={{ min: 50, max: 500 }}
                        step={50}
                    />
                </div>
                <div className="center donation_value--action">
                    {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <Button outline block pill size="large" onClick={this.toggle}>👉 Donate 👈</Button>
                </div>
                </CardBody>
                <CardFooter>
                <Img 
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/UPI-Logo-vector.svg/1200px-UPI-Logo-vector.svg.png"
                    height={50}
                    width={100}
                ></Img>
                <span>payments only !</span>
                </CardFooter>
                <Modal backdrop open={this.state.showModal} toggle={this.toggle}>
                    <ModalHeader>
                        🎉 Thank You !
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <FormInput type="text" innerRef={this.donor_name_ref} placeholder="leave us your sweet name here"></FormInput>
                        </FormGroup>
                        <div className="donation_area--qr">
                                <span>Please scan the following QR Code</span>
                                <br />
                                <br />
                                {UPI_IMAGE}
                                <br />
                                <br />
                                <h6 style={{ display: 'inline-block'}}>{'UPI ID:'}&nbsp;&nbsp;</h6>
                                <span style={{ 
                                    fontWeight: "bold", 
                                    borderBottom: "2px solid red"}}
                                >
                                    {"flood-relief-helpify@icici"}
                                </span>
                        </div>
                        <div style={{width: "70%", margin: '0 auto' }}>
                            <ReCAPTCHA 
                                    sitekey={'6Lc-bbIUAAAAAMgF56DG4vB4juXTZZPVpyDm_MSn'}
                                    onChange={this.onVerify} 
                                    size={"normal"}
                                >
                            </ReCAPTCHA>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <h6>Feel free to talk to us through the chat window below !</h6>
                    </ModalFooter>
                </Modal>
        </Card>
        )
    }
}

export const DonationBox = withFirebase(DonationBoxComponent);