import React, { Component } from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    FormInput,
    FormCheckbox,
  } from 'shards-react';
import { FaPhone, FaTimes } from 'react-icons/fa';
import ReCAPTCHA from 'react-google-recaptcha';

import { getState } from './AffectedZones';
import { withFirebase } from '../persistance';

const getEmergencyContactList = (emergency_contacts = {})  => {
    const emergency_contact_list = [];
    for(let [key, info] of Object.entries(emergency_contacts)) {
        emergency_contact_list.push(<ListGroupItem key={key}>
            <div className="emergency_contact--item"> 
                <span className="emergency_contact--item_provider">{info.contact_name}</span>
                <span className="emergency_contact--item_state">{info.locality}</span>
                <span className="emergency_contact--item_state">{getState(info.state)}</span>
                <span className="emergency_contact--item_call_btn"><a href={`Tel:${info.contact_phone}`}><FaPhone />&nbsp;Call</a></span>
            </div>
        </ListGroupItem>)
    }
    return(
        <ListGroup style={{ maxHeight: 500, overflow: 'scroll' }}>
            {emergency_contact_list}
        </ListGroup>
    )
}

export class EmergencyContactsContainer extends Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            emergency_contacts: null,
            total_contacts: 0,
        };
        this.contact_name_ref = React.createRef();
        this.locality_name_ref = React.createRef();
        this.contact_phone_ref = React.createRef();
        this.toggle = this.toggle.bind(this);
        this.handleChangeState = this.handleChangeState.bind(this);
        this.onVerify = this.onVerify.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        this.setState({ loading: true });
        this.props.firebase.emergency_contacts().on('value', snapshot => {
          this.setState({
            emergency_contacts: snapshot.val(),
            loading: false,
            isVerified: false,
            total_contacts: snapshot.numChildren(),
          });
        });
    }
    toggle() {
        this.setState({ showModal: !this.state.showModal });
    }
    handleChangeState(e, state) {
        this.setState({ stateSelected: state });
    }
    onVerify(isVerified) {
        this.setState({ isVerified })
    }
    handleSubmit() {
        if( this.state.isVerified && 
            this.state.stateSelected && 
            this.contact_name_ref.current.value.length &&
            this.contact_phone_ref.current.value.length &&
            this.locality_name_ref.current.value.length 
            ) {
            this.props.firebase.emergency_contacts().push().set({
                contact_name: this.contact_name_ref.current.value,
                contact_phone: this.contact_phone_ref.current.value,
                locality: this.locality_name_ref.current.value,
                state: this.state.stateSelected,
            });
            this.setState({
                showModal: false,
                stateSelected: null,
                isVerified: false,
            });
        }
    }
    render() {
        return(<Card>
                <CardHeader>
                    <h4>☎️ People Who Can Help</h4>
                    <span>These are some awesome folks who are ready to help</span>
                </CardHeader> 
                <CardBody className="card_height_fix">
                    {this.state.total_contacts > 0 ? getEmergencyContactList(this.state.emergency_contacts) : null}
                </CardBody>
                <CardFooter>
                    <Button outline onClick={this.toggle} theme={"danger"}>Add More Contact</Button>
                </CardFooter>
                <Modal open={this.state.showModal} toggle={this.toggle} backdrop>
            <ModalHeader>
              Add new affected area
              <span className="close_icon_card" onClick={this.toggle}><FaTimes /></span>
            </ModalHeader>
            <ModalBody>
                <Form>
                  <FormGroup>
                    <label htmlFor="city_name">Contact Name</label>
                    <FormInput type="text" innerRef={this.contact_name_ref} placeholder="your sweet name"></FormInput>
                  </FormGroup>
                  <FormGroup>
                  <FormGroup>
                    <label htmlFor="city_name">Contact Phone</label>
                    <FormInput type="text" innerRef={this.contact_phone_ref} placeholder="phone number"></FormInput>
                  </FormGroup>
                  <label htmlFor="city_name">City/Area/Locality</label>
                    <FormInput type="text" innerRef={this.locality_name_ref} placeholder="enter a city, locality, landmark"></FormInput>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="state">Select State</label>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'ka' }
                      onChange={e => this.handleChangeState(e, "ka")}
                    >
                      Karnataka
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'mh'}
                      onChange={e => this.handleChangeState(e, "mh")}
                    >
                      Maharashtra
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'ga'}
                      onChange={e => this.handleChangeState(e, "ga")}
                    >
                      Goa
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'kl'}
                      onChange={e => this.handleChangeState(e, "kl")}
                    >
                      Kerala
                    </FormCheckbox>
                  </FormGroup>
                </Form>
                <ReCAPTCHA 
                  sitekey={'6Lc-bbIUAAAAAMgF56DG4vB4juXTZZPVpyDm_MSn'}
                  onChange={this.onVerify} 
                />
            </ModalBody>
            <ModalFooter>
              <Button onClick={this.handleSubmit}>Submit</Button>
            </ModalFooter>
          </Modal>
        </Card>);
    }
}

export const EmergencyContacts = withFirebase(EmergencyContactsContainer);