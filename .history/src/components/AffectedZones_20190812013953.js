import React from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

import { withFirebase } from '../persistance/index';

import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
    Badge,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    FormInput,
    FormCheckbox,
  } from 'shards-react';

import { FaTimes } from 'react-icons/fa';

export const getState = (state_code) =>{
  switch(state_code) {
    case 'ka': return 'Karnataka';
    case 'mh': return 'Maharashtra';
    case 'ga': return 'Goa';
    case 'kl': return 'Kerala';
    default: return 'NA'
  } 
}

const getSeverityText = (severity) => {
  switch(severity) {
    case 'danger': return 'Too Dangerous';
    case 'warning': return  'Medium Severity';
    case 'success': return 'Safe To Travel';
    default: return 'NA';
  }
}

const displayAffectedAreas = (affected_areas = {}) => {
    const affected_areas_items = [];
    for(let [key, value] of Object.entries(affected_areas)) {
        affected_areas_items.push(<ListGroupItem key={key}>
            <div className="affected_areas">
              <span className="affected_areas--city_name">{value.city} - {getState(value.state)}</span> 
              <span className="affected_areas--severity_indicator">
                <Badge theme={value.severity}>{getSeverityText(value.severity)}</Badge>
              </span>
            </div>
          </ListGroupItem>);
  } 
    return (
      <ListGroup>
        {affected_areas_items}
      </ListGroup>
    )
}


class AffectedZonesContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      lodaing: true,
      affected_areas: {},
      total_areas: 0,
      showModal: false,
      stateSelected: null,
      severity: 'danger',
    };
    this.city_name = React.createRef();
    this.toggle = this.toggle.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.handleChangeSeverity = this.handleChangeSeverity.bind(this);
    this.onVerify = this.onVerify.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.setState({ loading: true });
    this.props.firebase.affected_zones().on('value', snapshot => {
      this.setState({
        affected_areas: snapshot.val(),
        loading: false,
        isVerified: false,
        total_areas: snapshot.numChildren(),
      });
    });
  }
  toggle() {
    this.setState({ showModal: !this.state.showModal });
  }
  handleChangeState(e, state) {
    this.setState({ stateSelected: state });
  }
  handleChangeSeverity(e, severity) {
    this.setState({ severity });
  }
  onVerify(isVerified) {
    this.setState({ isVerified })
  }
  handleSubmit() {
    if( this.state.isVerified && 
        this.state.stateSelected && 
        this.state.severity &&
        this.city_name.current.value.length 
        ) {
      this.props.firebase.affected_zones().push().set({
        city: this.city_name.current.value,
        state: this.state.stateSelected,
        severity: this.state.severity
      });
      this.setState({
        showModal: false,
        stateSelected: null,
        severity: 'danger',
        isVerified: false,
      });
    }
  }
  render() {
    return (
       <Card>
          <CardHeader className="hlp-card__header">
            <h4>⚠️ Affected Zones</h4>
            <span>last updated 10 minutes ago</span>
            <span className="close_icon_card" onClick={this.toggle}><FaTimes /></span>
          </CardHeader> 
          <CardBody className="card_height_fix">
              {this.state.total_areas > 0 ? displayAffectedAreas(this.state.affected_areas) : null}
          </CardBody>
          <CardFooter>
            <Button outline danger theme={"danger"} onClick={this.toggle}>Add affected area</Button>
            <Modal open={this.state.showModal} toggle={this.toggle} backdrop>
            <ModalHeader>Add new affected area</ModalHeader>
            <ModalBody>
                <Form>
                  <FormGroup>
                    <label htmlFor="city_name">City Name</label>
                    <FormInput type="text" innerRef={this.city_name} placeholder="enter a city, locality, landmark"></FormInput>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="state">Select State</label>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'ka' }
                      onChange={e => this.handleChangeState(e, "ka")}
                    >
                      Karnataka
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'mh'}
                      onChange={e => this.handleChangeState(e, "mh")}
                    >
                      Maharashtra
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'ga'}
                      onChange={e => this.handleChangeState(e, "ga")}
                    >
                      Goa
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.stateSelected === 'kl'}
                      onChange={e => this.handleChangeState(e, "kl")}
                    >
                      Kerala
                    </FormCheckbox>
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="state">Select Severity</label>
                    <FormCheckbox
                      checked={this.state.severity === 'danger'}
                      onChange={e => this.handleChangeSeverity(e, "danger")}
                    >
                      <Badge theme={"danger"}>{"Too Dangerous"}</Badge>
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.severity === 'warning'}
                      onChange={e => this.handleChangeSeverity(e, "warning")}
                    >
                      <Badge theme={"warning"}>{"Medium Severity"}</Badge>
                    </FormCheckbox>
                    <FormCheckbox
                      checked={this.state.severity === 'success'}
                      onChange={e => this.handleChangeSeverity(e, "success")}
                    >
                      <Badge theme={"success"}>{"Safe To Travel"}</Badge>
                    </FormCheckbox>
                  </FormGroup>
                </Form>
                <ReCAPTCHA 
                  sitekey={'6Lc-bbIUAAAAAMgF56DG4vB4juXTZZPVpyDm_MSn'}
                  onChange={this.onVerify} 
                />
            </ModalBody>
            <ModalFooter>
              <Button onClick={this.handleSubmit}>Submit</Button>
            </ModalFooter>
          </Modal>
          </CardFooter>
      </Card>
    )
  }
}



export const AffectedZones = withFirebase(AffectedZonesContainer);