import React from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
  } from 'shards-react';

import contacts from '../data/emergency_contacts';

const getEmergencyContactList = () => {

}

export const EmergencyContacts = () => {
    return(<Card>
        <CardHeader>
            <h3>☎️ Emergency Assitance Contacts</h3>
        </CardHeader> 
        <CardBody>
            {getEmergencyContactList()}
        </CardBody>
        <CardFooter>
            <Button outline>Add More Contact</Button>
        </CardFooter>
    </Card>);
}