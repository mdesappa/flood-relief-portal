import React from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
  } from 'shards-react';
import Img from 'react-image';
import { FaPhone } from 'react-icons/fa';
import contacts from '../data/emergency_contacts';

const getEmergencyContactList = () => {
    const emergency_contact_list = contacts.map(info => (
        <ListGroupItem>
            <div className="emergency_contact--item"> 
                <span className="emergency_contact--item_provider">{info.name}</span>
                <span className="emergency_contact--item_state">{info.city}</span>
                <span className="emergency_contact--item_state">{info.state}</span>
                <span className="emergency_contact--item_call_btn">CAll <FaPhone /></span>
            </div>
        </ListGroupItem>
    ));
    return(
        <ListGroup style={{ maxHeight: 500, overflow: 'scroll' }}>
            {emergency_contact_list}
        </ListGroup>
    )
}

export const EmergencyContacts = () => {
    return(<Card>
        <CardHeader>
            <h4>☎️ Emergency Assitance Contacts</h4>
        </CardHeader> 
        <CardBody>
            {getEmergencyContactList()}
        </CardBody>
        <CardFooter>
            <Button outline>Add More Contact</Button>
        </CardFooter>
    </Card>);
}