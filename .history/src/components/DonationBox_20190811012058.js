import React, { Component } from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    Slider,
  } from 'shards-react';  
import Img from 'react-image';

export class DonationBox extends Component {
    constructor() {
        super();
        this.state = {
            donationValue: 50,
          };
        this.handleDonationValueChange = this.handleDonationValueChange.bind(this);
    }

    handleDonationValueChange(newValue) {
        this.setState({ donationValue: newValue });
    }

    render() {
        return(
            <Card>
                <CardHeader>
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <h3>💰Donate</h3>
                    <span>last donated 20 seconds ago</span>
                </CardHeader> 
                <CardBody>
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                <p className="donation_area--desc">
                    💚 You're precious donations could help save lives by providing 
                    <span className="donation_area--desc_item">Clean water</span>
                    <span className="donation_area--desc_item">Food</span>
                    <span className="donation_area--desc_item">Snacks</span> 
                    <span className="donation_area--desc_item">First Aid Kits</span> 
                    to the afflicted zones.
                </p>
                <div>
                    <h5 className="donation_value--text">I'd Like to Donate</h5>
                    <span className="donation_value--rupee">₹{this.state.donationValue}</span>
                </div>
                <div className="center donation_value--slider">
                    <Slider
                        style={{ marginTop: "-5px" }}
                        pips={{ mode: "steps", stepped: true, density: 100 }}
                        onSlide={this.handleDonationValueChange}
                        start={this.state.donationValue}
                        range={{ min: 50, max: 500 }}
                        step={50}
                    />
                </div>
                <div className="center donation_value--action">
                    {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <Button outline block pill size="large">👉 Donate 👈</Button>
                </div>
                </CardBody>
                <CardFooter>
                <Img 
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/UPI-Logo-vector.svg/1200px-UPI-Logo-vector.svg.png"
                    height={50}
                    width={100}
                ></Img>
                <span>payments only !</span>
                </CardFooter>
        </Card>
        )
    }
}