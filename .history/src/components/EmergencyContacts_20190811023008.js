import React from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
  } from 'shards-react';
import Img from 'react-image';
import { FaPhoneSquare } from 'react-icons/fa';
import contacts from '../data/emergency_contacts';


const CallBtn = (<Img 
    src='https://p7.hiclipart.com/preview/620/23/820/iphone-telephone-computer-icons-clip-art-free-telephone-icon-thumbnail.jpg'
    height={50}
    width={50}
   />);
const getEmergencyContactList = () => {
    const emergency_contact_list = contacts.map(info => (
        <ListGroupItem>
            <div className="emergency_contact--item"> 
                <span className="emergency_contact--item_provider">{info.name}</span>
                <span className="emergency_contact--item_state">{info.state}</span>
                <span className="emergency_contact--item_call_btn"><FaPhoneSquare /></span>
            </div>
        </ListGroupItem>
    ));
    return(
        <ListGroup style={{ maxHeight: 500, overflow: 'scroll' }}>
            {emergency_contact_list}
        </ListGroup>
    )
}

export const EmergencyContacts = () => {
    return(<Card>
        <CardHeader>
            <h3>☎️ Emergency Assitance Contacts</h3>
        </CardHeader> 
        <CardBody>
            {getEmergencyContactList()}
        </CardBody>
        <CardFooter>
            <Button outline>Add More Contact</Button>
        </CardFooter>
    </Card>);
}