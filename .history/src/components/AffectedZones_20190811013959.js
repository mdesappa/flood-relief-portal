import React from 'react';

import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
    Badge,
  } from 'shards-react';

import affected_areas from '../data/affected_areas';

const displayAffectedAreas = () => {
    const affected_areas_items = affected_areas.map(info => (
      <ListGroupItem>
          <span class="affected_areas--city_name">{info.city}</span> 
          <span class="affected_areas--state">{info.state}</span>
          <span class="affected_areas--severity_indicator">
            <Badge theme={info.severity}>Danger</Badge>
          </span>
        </ListGroupItem>
    ));
    return (
      <ListGroup>
        {affected_areas_items}
      </ListGroup>
    )
}

export const AffectedZones =  () => (
    <Card>
        <CardHeader className="hlp-card__header">
        <h3>⚠️Affected Zones</h3>
        <span>last updated 10 minutes ago</span>
        </CardHeader> 
        <CardBody style={{ maxHeight: 400, overflow: 'scroll'}}>
            {displayAffectedAreas()}
        </CardBody>
        <CardFooter>
        <Button>Add affected area</Button>
        </CardFooter>
    </Card>
)