import React from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
  } from 'shards-react';
export const EmergencyContacts = () => {
    return(<Card>
        <CardHeader>
            <h3>☎️ Emergency Assitance Contacts</h3>
        </CardHeader> 
        <CardBody>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
        </CardBody>
        <CardFooter>
            <Button outline>Add More Contact</Button>
        </CardFooter>
    </Card>);
}