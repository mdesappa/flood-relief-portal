import React from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    ListGroup,
    ListGroupItem,
  } from 'shards-react';

import contacts from '../data/emergency_contacts';

const getEmergencyContactList = () => {
    const emergency_contact_list = contacts.map(info => (
        <ListGroupItem>
            <div className="emergency_contact--item"> 
                <span className="emergency_contact--item_provider"></span>
                <span className="emergency_contact--item_state"></span>
                <span className="emergency_contact--item_call_btn"></span>
            </div>
        </ListGroupItem>
    ));
    return(
        <ListGroup style={{ maxHeight: 500, overflow: 'scroll' }}>
        </ListGroup>
    )
}

export const EmergencyContacts = () => {
    return(<Card>
        <CardHeader>
            <h3>☎️ Emergency Assitance Contacts</h3>
        </CardHeader> 
        <CardBody>
            {getEmergencyContactList()}
        </CardBody>
        <CardFooter>
            <Button outline>Add More Contact</Button>
        </CardFooter>
    </Card>);
}