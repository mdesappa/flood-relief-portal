import React, { Component } from 'react';
import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
    Slider,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    FormGroup,
    FormInput,
  } from 'shards-react';  
import Img from 'react-image';

import ReCAPTCHA from 'react-google-recaptcha';

const UPI_IMAGE = <Img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAV4AAAFeCAYAAADNK3caAAAgAElEQVR4Xu2d0XYjO65Du///o3NXuU/fmTXHEuyNgiQnyKtMigRBiCU79u9fv359/frwv6+veQq/f/8eZqhsCTR0v5kdiaM2fxCY1TiFOeVVIh4VC+Ur5RfdL4ENzcG1uxSpwuui+D/2JdbNgJruKrwdTEwK3W5e4RXTMkG8wktQy9lUeCu8OXYxzxXeCi9jzgdZVXgrvKfRtcJb4T2Nk7fHU+Gt8N5OKtNhhbfCa1LofPMKb4X3NJZWeCu8p3Hy9ngqvBXe20llOpwKr/oYirn3W+b0DatrE/oxlET+NBbnY1E0DyfWWXET8VCfb5HwjRdTvlLMncOFYkdjpdxQ+9E83ijryy9VsVZ4J1AmCqkKkiAlzcOJleYxs6Ni9nK33PhCGivFvMI7/7z2jaV9yZWqY4W3wjtEQJHnJQY+eVHiIKA+aQ7KrsKrEHq+vuMAYZHOrVTvVHgrvBXeQOdVeBmoFV7xr5YMVm5Fidw7Xv4Ipk5tWk06nTocoLFSOxorxXyHYNFY6TWU2o/yitaYXos9NGn2L8OfkoiKUxVsBKDySwpGY7n2os1F83Bipc1FyUxzJDV8xabC+wpK/34N5bjqDxYNt1K9U+HtVUOvGnh/IewccSFDghIBemgpvwRWBxuaB4lT2ShssPAqxyqwZ+sU9BTgNMdPyoNiR7FRvEjEk/DpTFgUu9PyoE8n9GlI4Ub7TnEycdhVeCeoq0LfXRDaWIo49LE30VgqVorBjhwTsVJRcmpF83D2vLt31EFIe5nWQ+1X4a3wKi18uq6IhZwab+hWeMeIq1pVeBlbnQm7wlvhRaxTzYycVninsFGBVLWifjvxjv8VW2Fe4a3wIo1UxEJOK7wV3sl3pyjOORMo4auzX4W3wks4h7//Qm1Gp69eNfSqwRFCxctn685+Fd4KL+FchTc0ndM3c5zHfnrYOXv2zbXJb645ik66me6niKMeUUis1EbFSslM/VJs6H5XfnQ6pXa0VsrutHhUvCvXE9gorlL9oLg4+/2IiVcVjAJP7HYIFhXzxPRV4SWs+TybCu/4l80fPUD/ZTghZvQEUWKWiJW2goqViiT1S7Gh+1V4KXM+y67CW+GN3UeSVtghWFTMO/HOK5wQF8KpE20S2KghgQ5uFD9nv068FHVoV+HtHS+kzkeZVXg78XbinbSsmiJGpjsOkEQzO2p2WjxOLnfbJrBRXHUmUJK/s18nXoK4YbNDsHrVYBQMHlpOnTPRrvVa4e3Eiyde50RbPSnStqI5UjsaZ8rOEciEuNBD0qkHtXWwI/XsxGv8ci99w8YhuSoYEcmET0VGuudqzBNxKmzouiMeDidJvHQ/VY8KL6kG/2GCa7deNUwwp4SkQqfKrxpI2T9bpzlSOxJj0qbCywXEwY7UVPF/NSed/Sq8Fd4hAqkJizRdysYRD4oPzYXulxIsBzuCQSoPEstlU+GdfKPRY6z/Pb/o7lXDcwRSjU6JnrBzxIPiQ/Og+yn+UwFxsCMYpPIgsVR4X/iyElWwCm+FlzQfFUKylxogqHg6AlLh7ffxTrlc4R3DQxuW2lHRSdk54lHhHQtPol6qj1dz0tkP3/EmgJ35dEiuCnb3xEubWcVJ/a6uldrPqaXy/Wxd4Up8OjZOwzr7Elsaa4Krqo6JPQlm6unksU6/JIcGRO2cZlUFq/DSqjA7p5ZkR1p/stcrNlTMXvF992torAkRVHVM7EnxVLFWeCfIriadKtZJxKKEVNNAIkeFq5MLsaW8Inu5NjTWHXVM7EnxU5yr8FZ4KbewXSde/qYMBh0aVngZcBXeDR8noyevKhb1y6iTs6rwVngJuz6pP1SsnXg78ZIesGwqvBVeQiAlZicNJirWCm+Fl/SAZVPhrfASAikx+zbCS8DZYaMAp41+kt2Fa+K+bXWOzptrq2N1Gv2kWH9Kf+zQHrrndOKlTlfb/RRiVXjH//q9GpsehOt/SUQdhKt1x9mvwjv5noeTppY2+nmNvlrsE/t9pycQRwhX21Z4K7xDzrXR51+ulMBn9WFf4V0tuX/2q/BWeCu8AwTUo22F96yrnz0Synat8FZ4K7wV3qUcSEz1TP72WVV4K7xLm+47Pdp24u3ES6X795f6SAD1/CF26nGSpJFoSBKHa5PARsWUoOOOPGZ5rs7R2W81dk6silsnrVd44a9T0Maij1k7SLO66a4cE423Iw/KD1rnFK9WY5eoP8U0aVfhrfAO+bW66Sq8vNUrvBy7HZYV3gpvhXdx5yWmugrv4iKa21V4K7wVXrOJ3jWv8I4RS2Dzbn1WvL7CW+Gt8K7otP/aIyEunXgXF9HcrsJb4a3wmk30rnmFtxNvhbfCW+F9VznN11d4K7wR4U089iR8qv6heybsVKyJZlZ70vUdn5Ygse7A9FOwUXju+Cx7Ajuah8KnwjtBKCGgqULuEAlFrtF6okFoLDO7HZh+CjYKb8pzB/MEdjQPhU+Ft8KrOHL7eqJBbg8y9M8cKs5PwUblQQWrwquQDQjWbEs6fRpp/KJ7JuxUHg5hle+71z9FXHZg+inYKE5UeOcIdeINHCAVXkG6wBuaSgjIeoWXoPbHpsJb4cXsSQgoJaRKYodIqJh6x/s+Qp14xz8EqtBMYJfq1068nXgVn29fTzTI7UH2jteClAqWM0AkeEXzUOAtF14V0Gg9VRDqN1Fkio16tKN+aY4U0ytO+pQxy5HmQXFT9Ujk6MT6XWxpnSlf6X4Pnie+j9cJqMLL2oCSJyFYTiwJUUrwUVWJTkoOdiqm775O60wxp/tVeI3HSQf0RANQ8lR4E9XY8+ZSJpPP8Up7kvYO3a/CW+GddhUlFiVyrxoyXwT/OdLpRbqar3S/Cm+Ft8Lr9frQulcNIWAnbqkQ0kGB7lfhrfBWeEP6UOENAVvhHSPgnAR9c40Rlp7aveNleCurCq9C6P51qju0d+h+cuJ1HN8PK/e4Gli6n8qQ1iMVzyheFScVpRk+CZ+qHnSdxpqwU/fqCcxX81HlSHGl9a/wCuSUgCSm89MmUEIuhVuC6AmfJPdXbGisCTslShXe36+U9O3XTD/Hqxro7d02GdATluZP91PwnBZPJ15VsefrCQF1Pv+c4JUTD0N1bkXjodioHCq8E4Qo6BXe+ZRAhScxfakGSazT/BN2nXj5Z64dblR4K7wOf57aqgOLCkiFd/wFMnSiq/BWeG8XgL8O6QSqBKR3vM8RULhVeO8X0ApvrxpiAkodV3j5V+0RzCu8c9TowZOw68R74MQ7ow89YVVTJqZIumfi0ZYeAkQA/9rQWjl7UlsaK7WjcTp2iVgdn7Q/KJdT+1EMqJ3FAfrtZDTYFOj0kKDgpaYPGg/NnzZPIk41fX0S5qvrQftRYU6HD5q/sx/FgNo5PYC/FpIGW+Fd+9ivGqvC67QPs6W9Q8VM1Xh1T6b2o7hSO1b9P1YVXojeJ01fO4gFYcVfhP4TcqzwzocWygFqRzle4TWQq/Aa4E1MaRNQu0wWc6+JWB2fqQl0hEJqP4oBtXO404kXolfhhcAJM9oE1C6TRYWXTue9493QIA7oiULTeE4TgdPiobX6pMMukWPKZ2oC7cQ7rhieeFMkuLtYaqJRbzzcHQ8VD5VH4pBI+aSYUwxSwkL9JnDdwStaj9NiTfBRcaPC+8U+ZaCAHZFyB+kSezo+E0SngwAVwcuOcoDuSZ9cEnFS0b3sHO44+5KepPspzCu8FV7ELad5KrxjyCmu1A4V3zQ6LdYEHyu8giQUdAUsOV2pT9UHCaI7PinmKs+V10KdeGk1OvE+uEP/c40+2tGm2yFKNMfEoySneYboFd5eNVBOOtyhe9KepPspvarw9qoBcctpHnr4okCNu1gVp2ouEi/FldqRGF2b02JVdSb5Km5UeCu8hFfWGyQJoq9+OulVA6LNw6jCK64alGqTe0zaIE6zJvI4yWeKzLRBdtSKygDNke6n7Ch2lI8OdxKxUp+pg1DVi2pg5BcoKHj04zIKHErKRFMmfDrNQ+++TquV4gBpEMobGouqIx1aVDyUk9+lzxU+ZF1hU+GdoEoJmRAzVfyTYlWkSwmIwujZegI3EsdfG4qdc0hQDBKxUp+deMUdDm26HQWhhKzwsn9K2dE8iRpXeOcIfJenJdrnD57PPk5GT1EqkqcVJNGUCZ/qETVRx9NqRcUuVY9EPHRoUbFQDL5Lnyt8yLrCpsLbq4YhAqsbshPv/B3/Ci+fpIl4OjYV3tDnOFdPkYoEVCTp41InXlURtq4aduSV8tF5WkrESn3uOLRp78SuGhjluJUqlkNKHtVzSxXr3fspfykBTU1nI7/fBddEPRz+f9KB7uR5N69ULJGrBtXsd6+rplMg3B2PcxKujEVNCQpXGmuiHqlYEzlSMaM5OnjTWGkPOAePk2eFFzBdETJREBDmw0TFSv1SO4foiT2pz++Ca6IeDv8rvOwTOgrzTry006HddxEImP7DTJGS+P4uuFZ4fw/Lr2p8Eq9ULBVe0uWGjSKP4RqZJhpdBaJIqeyfrX8XXBP1cPDuxNuJ96iTkIhDrxr+oOYIwd13cbSOyo4KKLWbxePgXeGt8FZ4VbeD9USjqzAcIajwKnT/ve7gXeGt8FZ43+85aVHhlRChF1BcqV0n3rOepNRhF7njpafkJ93TzYieyl8Vk0yDOxqd1pnGmsANqfELRqtzVCEluLyjHhRXeqApjld4J8gmCOIQ4LR4RtCpOBUpiV8qEPQAVYJF1yk/FOY0HoortUvVg+Ja4aXMMewomSnplCCdFg8RyMtG5Un8UsxTjU5pRwWCckPFSXGldql6UFwrvIohgXVKZko6JUinxUMEssI7JyoVCMoN1TYJLtNYVX+kRJLwXMXaq4ZeNQwRUOQhhKzwVngrvBu+j5ee6OpkTqwnCOLkf1o8Fd77WUf5QbmhMujEO0aI1ury2Im3E28n3gECdOJXYpZ4JK7wZp4kErV6CO/1XsfIeeK0o6Q8jViJNwFSOSZipXV8kO73+P/xHb+reUzzSPQVjcXB+7Q8VsdD96vwinfYKZnppET3SzWP45dOCok9aYOoOtJ6JeKhsTh4n5bH6njofhXeCi/+aJfTsKtFgjZIhXde5QSuDjdWx0P3q/BWeCu8k2uPCm+Fl76Bpg6Q3vF+jb8EQ4FH7hRPegS/YlHi4ky2I1uKK42FTiYKG5pHIh4aC8VUcSclWPT9ikQ8tI6deDvxVng78WLtpcJD7VSg1O9quwpvhbfCW+FVejZc3yFYnXjhR4LU4xthgfOYdVo8JH/12EevNyg2qXokHhdpIz+mlkAPJHJUdaR7ruaV6o1EHmpPuh6543XITBKhDeAIFiUdye8VG9Vc5L414VPlctIUpfKnvFudo5OHsl3JK8WdCu8EIVrIlNCdFo8i12id5pEgKxUkdRDSWGk8CtOE30SOTh7KtsLLOrYT7+RTDQxS/ghK91OCRQ+tRNOpHFdPg87TWYV3jB49QBQ/VnPZiWcaa+Jfhh0yk0RpAziCRQlA8nvFJiGSCZ8qlwov+3lzR+gc2068itHP1zvxduIdMqfCO/+hQ3rgrz5cVB0rvEw8HasKb4W3wjtAwBEs+tRHRZDaXXE6tp14mfxOhTdBntU+GSx/rFZPJipWJQTKftU6nQQV5qvi/7uPk0ci1gQfHeE9DZ8E5imfFd4JsgmiO2St8KbaYHAPBz+nm4oywccKb6pac78V3grv7cz7LoeLk8ftoIaewCq8iUppnxXeCq9myZuvcATrpKneyeNNyF56eSfel2D6iBdVeCu8txPVEawK77gcFd7bqbrNYYW3wns7+Sq8t0P6cFjhzeC6w2uFt8J7O+8qvLdDWuHNQLrNKxbe1RGnHkETn2Gk2CjBohPPLB7qk9qpyW0Wq8KH4O7kQfZT+SdypHGqWFfXysljdQ+oWCu8xvexKnDfXVdNlxAJ6pPandbMTh7v1vfv63fsmYi1wjv/z8YpPrPvaqDFSth14uV3fKtPe+cAWd3MO0Rwx560J2nfKQ7QeBJ2tB4UmyuHTrydeIdcptcwqukoYZVf0pS06chenXgd1HK2lAOUxxXe0P+pU4ooYaEE6cQ7RiCBqar/jj1VTKN1Ki6KyzSehB2tB8WmwlvhjXxESTUdJazyS5qSNh3ZqxOvg1rOlnKA8rjCW+Gt8E6+nS4h9FfT0UbPSQ97Ilh9H5/Kn9bDEt4vaE3v/75LsSgJINyP7RKY76gHJTq9MtmRI+UHtfskXtF6ODkm9nT68XeFl1Kd2TnkcQpNov2kiY/imsqR4O3Y0Px3HOgJEXSwo31F7R6YV3idkr1v+0kNkhKlTrzv80ZZfBKvKrwVXsXn29c/qUEqvLeXP+bwk3hV4a3wxhph5PiTGqTCu5weeMNP4lWFt8KLiU4NP6lBKry0yuvtPolXFd4K7/IO+aQGqfAupwfe8JN4VeE1/mWYvkHiEGRUMCUQiViddzRpHrQrPyV/lZ+qs7I/ZT3RA1TMLrvV8dBYd8RJ+1xxFX9XQ6KZaWOoJBOx0oJQ0lFsVGPRPKidk4eqs+N7pe1qAVG4rY6H9sCOOCnPFeYVXvglObQglHSOMHzKwaNyVGRW9qesrxYQhdvqeGgP7IiT9rnCvMJb4R32QUKwHfFTZHZ8r7RdLSAKt9XxVHh7x4v/DZeehJR0jjAkBDSRv8pRCYiyP2V9tdAp3FbHQ3tgR5yU5wrzTrydeDvxLlbk1QKiRGB1PBXeTrydeA86eJT+KQFR9qesrxY6hdvqeCq8Fd4Kb4V3uR6vFroKLy/xR1010BONw8MtaRPQgjjYJO5qOXLcUgkB93yvpeIGzUP5HWVB91OorOZVKg+V52id5k/3u+wid7yOuDjJENtEEyR8XrlRgtB4CJ6v2JzWeKQhH80zeVqY4UDrQfdTNVnNq1QeKk9S51SsFd7JLxDQAyTVWKsbhBJZ2aXIrPZ9d13Vkeah/HbifbdS3utpXzm7VngrvA5/kC0VLLSZYaQEkuah/FZ4jaIB0wovAM01STRBwmevGtxKv2+v6ljh/XofVOOKBm32glGF9wWQ7n6Jai4yfSR8Vnjvrrz2p+pY4a3wahY9f0WvGnrVQLmD7ahg4Q2hYYU38wOrp9V/y8Sb+M01yPPY19Pt+OjXCAPVzDPsKGEpsRJ2V36JeqzGjXJc2dEaK7+JdcqPWSxOf1AOpPacxlPhXfu45BSZNiVtkIRdhXcugbTGCWFVPik/KryhX6BQBUtMg6tPO9ogFd5OvJSrtK9SdhVejmzk591pOI4oUTLTPSu819sDz/8Upr1qGLOV8or2nGNX4eXoVXgDb66lHqVoU9IGSdj1qqFXDan+WD18cdntVQN+Qy8hgqqQiT3p9EntKrwV3gpvhbfCu+HbyRzRVofTs/XEgUXieMWGxvqK77tfQ5+IKrxCeGmDUPLQQjp3ineTUflTsSr70fpqzGmcyi7BAYqNipWu0xzpfo4djXW13ZUj3ZPi4/Ty9I63wktLMrZzikXvsOiEsUOwaPNQu/srrD3+hFhpjtSuwmv8LzYFXYnZDgEZtZ+KVbft81fQHCnmNE5lR+OhdiqexPpPiJXmSO0qvBXeaa9WeOdSRhuP2iWEVfn8CbHSHKldhbfCW+FVyjNZp41H7YxQselPiJXmSO0qvBXeCi+WJP4GidOwRrjI9CfESnOkdhXeCm+FF8nRHyPaeNTOCBWb/oRYaY7UzuEOLaRzbTj9WkgHBJKMk8hsv8SnM1Zjo4iVyJ/U8LKhb/Q5OdL8Z3aKj06eo33VniM7JxbKZWrnYJ7gZCIPFWeFd/IvwwnBVgVJkJLmQWNNicDqeJQIOnlWeJ8joDBPcKDCC783QRWDCk/CTsVa4WVf09mJd/ylRYpzVHioXYLjKsfTerkTbydexdm3151JMDHx0HhULNRvQnicWKiAUrtE/oqkFd4JQoroClxy/0XJQ+1oDs79JyUdjTUlAqvjUXx08uxVQ68ahs92q8VFET3ReDRHakdzqPBy5KhAKj5Sv4mJz4mFcpnaJfJX7KDDh4Pr9Prr+gQPOX0TASmiK3A78T5HgJIugbfymeAA5aqKhfpNCI8TCxVQapfIX/GK9oCDKxbeBECpRBTw5HCZAge/TpH6VPkpkVD2z9ZPIyvJYZdNoh67clk50OzIMVErpXP9eXf4SQoqShXeHa21fs9EM6/PYr4j7QElSqvzTNRK5VjhrfAOef5dGmt1I1/7JZp5Rx50UEhcQ6TyT9SqwiuqRUGnokSJrEhH86DxfFJjKewS64l6JOJ0fNIeUKLkxERsE7VSOXbi7cTbiZd0a+hAD4QSc1nhHUNb4Q01CCUdnTBV96w+tTvxziuSqIfiwOp12gNKlFbnkaiVyrETbyfeTryBTk80cyBMy2WF15h4vyYMocDSqY6SVZ0uFrsGxolYqU+VH63jabiujsepRyJWJx7akzM7Gk8CG9UDiTycPSM/dkmL/EmFTMRKfSoCVHgVQs/XnXokxMWJh/ZkQrAS2LAK/7FK4TrFvBMvKxktFhVBFuUfK7rnjgY56e6Y1lhhTmvpxFPhHSOQwrXCS5k+saPFoiLopED3rPDyr6hMYEc5p7hDY6Xx0P1UHnSd5kH3exzMnXgZfLRYVARZlJ14HdxojTvxzlGv8FZ4cV/SpqzwziHvVcP6R2IqhIkewA1pGNI8jC078VLwaLEqvBXe1ZxT+1V4+ZWSwna0jn+BInFZT5NQIkjFjhIylQfFXOFD4l2NjYqRTsrU7ornJFydWGgt6Z6n9SPlAM3/cRVFv4+XioBqILKuADit0KMcVR4Uc8fv8MSefC0mqaFrk2geJUgn4erEovK8m6+n9WOCO4rPFV74m2sKWLKeah7Hb4V37Z3rahF8TF/wEKW8qvB24p0+LlJCEtF1H10pmWmsq7FRcSamFpUjFR765DKzc2JReXbifY6AhXmvGsYX65SQSiTuJrKaWhyCdOLtxHs3X+mQkOrHxKGtNKBXDb1qUBx5up5qAhSMeKMr1egnHWhOLLSWdM9UPU7ijoqlwlvhVRyp8A4QosLTq4ZLdt5/fKcHhCJ4J16FECiW8xieKjQhnYKGThHKb68aetVwN18pV1P9+OOFl04QTkFW70mLrASS+qV2s3gUpk69iAjQ/VQedHJ1/CoePFtX+dN4EgJKYyG4/LWhedD6P4bBk95co6ArYjkicffElxC6K0bql9o5mDr1qvC+LzEK70TfUV7RWN5H5T8WFd7Ar0GogtBCKzKvFIgKLz94nAOETjyUc4rLdEig8VDBonY0f2WXiEfpQyfexWJPJwFFHuqX2jmCpUipcn22viOPCu/9b5LRQ4BwplcN/yBAQXcaefWeCYHoxNuJlx4CijvUL+U57ccK72H/gqgKQgtNxZ4S0smDPkolcrzyoH7plE33o9xQOTp+FQ+erav8aTwJXtFYCC6deDvxPhBQDZIQnsRBoJrHyXOEwY48EtOgIyC942Xo0QOE1v/R67NPNbA0uBVtHqfR6Z5UBDk6md9OS+Tv5EhxTTSPyiOxZ8KnyiOxnjhcVZxKB+jBpPYl6xXewH+uUQKoAtKmTNipWBPr9JA4rR50UkrlkahVhXeOaoW3wjtkyI7m6cTLPimQEE/H5w7u0INpR6wV3gpvhRcqDH2S6MQLARdmFV6Ia+pRkjYIPQkpARRsiTwo5irWxDqN9bR6VHgT7OA/xUT73MmiE28n3k68sIPoQVjhhYB34l0PnENyaktPwtMmrNX5Z9jB/0nitHpUeDMMoXWmfe5k8ftrEi0N6LRHQkr01W/0ULwdAlCyJjBVeSRidfJYHY/CZ7SueLW6X1U8NM/Vdk79K7yBH/r7pAnTIQ9tdNogiVgrvOufJCq8v35VeCu8VAeX3w1XeFmplNB14mW4Onys8FZ4GesmVqrR6YYO0cmeKo/V8ZAcLhsnD/r05jxJ0DxX2zn1r/BWeG/nq2p0uqFDdLKnymN1PCSHCi9FTds59a/wVng1w958hRKsN939/8sdopM9VR6r4yE5VHgpatrOqX+Ft8KrGfbmK5RgvemuwksB+8dO1aN3vAzgmPCycPi3aO24F6LgKTIT7GgsZK+/Nqfd4VEMaD2o6Fz4ObakZnQ/iimJ8RWbBOfUvgl+OHo1nXhVMqP1BLAUOJUDJWUiHhqLypEShMbjYLN6TypmFV7OuoQ+qGgoJ1N8rPAu/s21GUFokRXpKrxjBCq8DnuYbYVXfI6XwdqrBopbhXf9F51UeClbuV2Ft8I7vaejkyKlZIW3wkufiHaIGeX5jlh71XDQo726p6vwfqHeoiTfUY9OvKjEllGFtxNvJ97Jx+noBF7h1f8tRpSLHhK0jiTGV2wqvOLHLmmhVz8upRqdEiRhd9o0+EqDkdfQWlKuOqJEYyW47Ki/itPBTvl+tq7wTsST6uXpF6FTMld42e9mpYil/I7qlSCyarhErLR5UrEqv3fXg2Kq4lzND5VHIh7KHRVrhRf+AgU9lKjdjoknQWTVzIqwRJRo86RiVX5Jjqvfj3D4SPNX3EjwlXJHxVrhrfAO+yBBZNV0irBElGjzpGJVfkmOFV7+iRiKnTNEVXgrvBVeqIT0kIDbHfVGcCde71/GK7wV3govVMIKL/u4IYTb+l7hxJ6deOE3jKlTmz6iJuxUrPRxaWbXq4Z5u1Z4K7wjhihu4K+FpE2pAiInE43l2ovGQ087J9bV4kqxUTVMYJCINRGnwoauO/mv5rITK8WH2lFs1H4VXjgt04KkmpmSmeahiLV6kqb5r47TwS1x8KonKfr0loo1hd/Ib6o/KrwV3iGXE2KmGp02ViLW1CFJc0yJGRUXik+iVglMFVedPCq8Fd4K7wABKiwpEajwrkeWHkoq0gpvhbfCW+FFHKAHkzMpKkG7e73CO0GUEuBySUlAC+LEmph4aB4OwRMY0Dr2jpd/HpXWMVErh4+UA04enXg78WEd88oAAA3NSURBVKJpxyE6bdjEwUObzsk/YeuIAD18aR2dWBPYUQ44eUT+gYIWkgKgikEBosRS8dB1mkcC10QsFJfLLlErlSPdk35SQMUzwo/GqepB81h9gKb44eRR4Q1dYSjSknXaeBVegra+hqKCRgWL1p/GqVCjeTiCpWJaffiM9lO1qvBWeBGXFbGQU8MoIS4qR7onFSwVz2rRoXlUeENfhN6rBkNBJqa08TrxsnoovCu87HunK7wV3mlH0sZiba6tlBBoD/9+Bc0xEQuJ/68NzcMRAbonnRQp5jROVQ+ah4O5imn11N+rhsCkmCIsJQ9tvE68DHGFN+UHFSwVz2rRoXlUeDvxduI97FefmUT2Uw2Jw1XVosI7Rkgdkn1zbcIuOtEowtJ1VUzil+aYiIXE36sGjRqtsfJc4TWE92tSFQpsws450Wk8inj0fifxSJjIcYe40jdmV9spbtB4lF/CHaeOCdGm8SRioXhfdjSPh22F9/4vc6YFcYhV4R3XkWJD63g1VoWXT4PkcHEElNo6/KjwwjvOxBsEFV4uWFToqJ1q1pRfIkqOQDicPOmJUNWLrDu4VngrvEPOOcQiRHYmRSp01E7ll/Jb4VXIr1t3+qPCW+Gt8A4QcBqrwturhulTce94e8d79yOhM3NQwVptp3Kk8Si/nXgpQvfbOQdzJ95OvJ14O/EOOdA73vsn98sjFl56ftBJwCEAPZlorBQbZUcxSOSvYp0+ZsHvQHb2JJOi2o/iOvObqDH1+RCISa0cv+QpS+1HY03YSe7QqwblmBCdAqBioQ1S4b3/GkY1s6rl3euqmVcfIDSeHb1DY6WYqv0oBgk7xdNOvBOEKrwVXioSqvHIYEJjUYK1w28n3klV6KRIH6XoyaNITvOo8FZ4qSgpTlZ4nyPgaAC1Tdip+nfi7cQ7RMCZlFYLliL63UKXujKhmFPxULil/Hbi7cSLhIdO0Yro9GkhIXRUBFSOO7Cr8Kqq3D+Bkh0doae2CTuVeyfeTrzo4FHEShwEzp4VXoYeFSW2m/cpChprwk7lj4U3MQ3RSUjFshpYFY8qyt3rCVwppk5uNI/EU4S6aqAcSOCqcDvpvYxULCm/lM8V3sk/UCjCJqYoWsjEhEnJSkVH5U7rUeEd/zbahQ2ts6oXWU/FkvJLcnwc2vRzvInmoo2lYqFTRCoeWixql8iDYkpzUBMm9au4Qw806jeBq6r/SaKUiiXll/KuwtuJF93xJgRCkVgJiLJ/tk4FUh0E1G8CV4XbSaKUiiXll3CuE2/oMYs2HS2islONR65MEgKRyqNXDb1qqPBOuiAhEM5kkopHCczd64k8KrzeO/CjGidwVfU/SZRSsaT80l7tVUOvGnrVALtntUjSJ6kK71lvIFpXDZCr03dQqU9FLOqXnpLUTsWZ8qv2ffdudEc96JtgJPcTbagoqyfC1DUNmfgV7on+cHCdcpJ+qkGBQO4Nqc8djU6nHSfWBLEo5jtioU3gYE7xWW1Hsanwzivl4FrhhV1AxYXaqTBTftW+nXgJQmttHIGgB5OzZyfeCXq0IJ/0eEJj7cTLfk7dkSPa6AkeO3kkbCk2nXg78WI+phqLTpjUTgGQ8qv27cRLEFprU+HNvIHm4NqrBtgDVOionQoz5VftW+ElCK21cQSCDi7Onr1q6FXDsEOo0FE71aopv2rfCi9BaK2NI4IV3nGtHFynE+/1z1t3UyQRLL1T/Sl3WNMiB36wkDbr3Vz7RH+JAzTh88KW+qV2O+qZ0CuVx/W/hBXeAUqJgjiCReNxDq3EI6Ei5XdfT4hSwmeFN8fECu8EWyp0dPpUZabxVHgVsmvXEyKZ8FnhzfGiwlvhRexyJne04TcySohkwmeFN0e6Cm+FF7GrwotgexglRDLh04k1FQ9HfWxJnySdWCq8FV7Enwovgq3Ce/36wuTNXo4qt6zwTrBz7ilpoRMFobGo6YPeK9McnTx4i3wPy8Q0mPCpOEd78jTu0B5w2Dj9WkjH8Um2qwmypZCLp4gdjU45lao/xYDazfJ3fDq2o5ioz1TvULGPxTP7djJK9NPsUo1HSJfChhKLxkMbS+2XIHqq/hQDalfhVewZr9P+SPDxirIT7zf5InRKLErlhHioR1saa4V3/U//UH7EhA4+Ecbi6cR7/7dspYpF73GpYKUmLOqX5lHhrfDSwSTVy514O/EiPaMTjdosQfQKb4W3wqs6L7Cearze8T5HgJK8Vw38o1bOQejYkh6g/ehIA+VkYhDoHe+GD7M75OlVA0OPNrpqVipY1I5e0aTySMQTE7rT7ngTX5LD2oNbOcVSpLz7RKdkfZySi8mT2i8hhFTMHO7Qg5DuSeuhOotip/ySdScWakvtSH5/bSL/ueYERGwpkVNiRhtE5ZHyO8I8tV+Fl30hIK2H6qkdwpMYaGge1E7hOj2YO/HO33hIEIT4TB0SdGpLTe60CRJ2VmN90HcgU+wcfEgPqIOH5kHtnPw78QYe3xVBCOkqvPOrFto86imDNhed6hMHocqBYqf8knUnFmpL7Uh+vWr4B4GESCZ8VngrvJRXSiB2CA8ZPlT+NA9qp3DtVcMEAVXMBEGIzwpvhZdyVQnEDuEhPaDyp3lQO4VrhbfCiziiiE6aRx0gtAkSdgi0F56k6PUGrYfKg2Kn/JJ1JxZqS+1Ifr1qeKFBZsAmiqUakjae8jvKM7Ufvf9cbWc1Vt9cQ/A5fUVtqR1K8K/uzD7VQBvWCYiIQCrOhPBQnxcuNM/TBIvGQ3lF91O1ovWYPoJCwVaxUuxOypHmsOMpS8U6/VRDAnQVUIV3jBCtR0J4aCxOE6zmjhIzB4O7ea5ipdidlCPNweEc7R0Va4V3ghAlc+rRhTYBJQ+1k6SDU53yu1rMaD068Y4/O5/AtMJLO0f8u+yOYq2+/+1VAycPPUDUwZvgXSpWit5JOdIcKrwGcpSQxpaR70ZQzUwFffUU5TTk6lrS/VStHAxWT+e0D07KkeZQ4TWQo81jbFnhDV0JrK4l3a/Cy9/QTQwCqV6mV4POodQ73t7xDhGggqUaJOV39RTpNN7qWFVNRusn5Uhz+FYTr5oGCEhbTp7AVJfAhuD514biSvdUzXoaPitFh2KqxMO5oqL1WM0rBzsHn8S+eOKlxaIApKakhN8ENk7xVzdIhdep1tiW8ipVj9W8yqCauU5RsVZ4O/Eqjry9nmr0twMxDVQepvu3zSu8b0P2ksGOOld4K7wvkfOdFykiUwF5J4Y7XqvyuGOPd3xQ3FQeCb/U5zt43PVahc9d+/y3nwpvhfd2Xikif0pTqjxuB044pLipPBJ+qc/VmF77KXwSMVV4K7y380oR+VOaUuVxO3AV3tWQPvbbUecKb4X3drIrIld4GeQUt1Q9+uYaq+Nl9SOElxKWwqqIPvLrxEmbgMZKsVF2FAOaR+JTLY/Ggj8ppfAZrdP86X7KLoHrakzVNOzkWOFVDALrtAkcYlV47//lXlrHCi//tZBZuzn9AdpYXkNUeL/mTbe6YLRhnTgrvBVeKi4JO0eUEk+ENMdUX3XipRWZ2FV4Oaj08ElgTn124u3Eq7hT4eUaMbRUoCdO9NTJHIBn6rLCyxCnnGO7aatOvOIpnP70D22QWclS4pGIleaRusNKYadb7N5X0FpR4UkIRCfeTryKj51479UNeSFf4Z0DXuFlhFSNzrxyq8SBRrnBs5h/xtfJ8UcILwWeFppOnzTOy47uudpO5UgFhDYBrbHKgz4R7YiHxpoYIigfd/SAw4EK7wQ92gQOeWgx6Z6r7VR+Fd7x75Ep7BLriXpQoVf9eBqXpwfTT7jjpYRUhR75pQSgce447VM5Jho9FSut12nxUCHsxEsZ8EP+c43CU+FlvwZLcVMHCG3004TutHgqvONPIDhc7sQLlZeCvqOx6J6r7VQpOvH2qmHEEdWPp3G5wqu6fbCuCt2rhucIUNw68a7/jgfVGomDkE7YilcVXlXNwXoCOBjKw0wVusJb4SX8ojwne7k2Fd7MEwj+VINb0Hft6ceF3t3n1dfTeKiYq7gSDXKaQCTiobipelB+KL+JdRpristkoHEGJTqBO7Wo8EL0TiMrFZDT8qBNQEWA4qZoQ3FVfhPrNFaKOc1B1SoRj9qT5lLhhcidRlZKkNPyqPBCQhpmn8IBxfEKr0GCkSklRyCUh0saT4IcVzyKlHfjmsqjwpti7NjvaVzuVYP4ntuVFKHkSMVI40kJVoWXvQlCcVO8ovxQfhPrNNYUlyu8Fd4hz08jKxWQ0/LoxJuQ1rnPT+GA4njiIFB70mr1jhcidxpZKUFOy6PCCwlpmH0KBxTHv43wGrVcaqoKMguGFsvZk4JzUoMkPtqVuqteLea0vsqOco5yXMVD1yl3VP60P2geDq7TiZcGtNpOFaTCy+4/dwgWrSVtAioCqzm+41BK5UgxV9yo8KYqNvCrClLhrfCSN2yomKfoT3n+SXk44unYkpo5uHbi/c1EiTYBKfBfG0oshyCrBYviSnOk05dTR2q7Ghsap7KjmKv8aX+oeEfrlHOXvwpvhRfxjjaP2kw1191NkMpD5UnWV2NDYnzFhmKu8q/wvoL+ja9RBelVA5vqe8d7P24O7SnPncnMiffupyWVf4U3Ua2JT1WQCu/9AkKnFkUNWksqLqk8VJ5kfTU2JMZXbCjmKv8K7yvo3/gaVZAKb4X37unrRvq+7IrynB5KLwf25gsrvL9+/R/5021apdu/kAAAAABJRU5ErkJggg==" height={300} width={300}></Img>;
 
export class DonationBox extends Component {
    constructor() {
        super();
        this.state = {
            donationValue: 50,
            showModal: false,
            isVerified: false,
          };
        this.handleDonationValueChange = this.handleDonationValueChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.donor_name_ref = React.createRef();
    }
    handleDonationValueChange(newValue) {
        this.setState({ donationValue: newValue });
    }
    toggle() {
        this.setState({ showModal: !this.state.showModal });
    }
    onVerify(isVerified) {
        this.setState({ isVerified })
    }
    render() {
        return(
            <Card>
                <CardHeader>
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <h4>💰Donate</h4>
                    <span>Last donated 20 seconds ago</span>
                </CardHeader> 
                <CardBody className="card_height_fix">
                {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                <p className="donation_area--desc">
                    💚 Your precious donations could help save lives by providing 
                    <span className="donation_area--desc_item">Clean water</span>
                    <span className="donation_area--desc_item">Food</span>
                    <span className="donation_area--desc_item">Medicines</span> 
                    <span className="donation_area--desc_item">First Aid Kits</span> 
                    in the afflicted zones.
                </p>
                <div>
                    <h5 className="donation_value--text">I'd Like to Donate</h5>
                    <span className="donation_value--rupee">₹{this.state.donationValue}</span>
                </div>
                <div className="center donation_value--slider">
                    <Slider
                        style={{ marginTop: "-5px" }}
                        pips={{ mode: "steps", stepped: true, density: 100 }}
                        onSlide={this.handleDonationValueChange}
                        start={this.state.donationValue}
                        range={{ min: 50, max: 500 }}
                        step={50}
                    />
                </div>
                <div className="center donation_value--action">
                    {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                    <Button outline block pill size="large" onClick={this.toggle}>👉 Donate 👈</Button>
                </div>
                </CardBody>
                <CardFooter>
                <Img 
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/UPI-Logo-vector.svg/1200px-UPI-Logo-vector.svg.png"
                    height={50}
                    width={100}
                ></Img>
                <span>payments only !</span>
                </CardFooter>
                <Modal backdrop open={this.state.showModal} toggle={this.toggle}>
                    <ModalHeader>
                        🎉 Thank You !
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <FormInput type="text" innerRef={this.donor_name_ref} placeholder="leave us your sweet name here"></FormInput>
                        </FormGroup>
                        <div className="donation_area--qr">
                                <span>Please scan the following QR Code</span>
                                <br />
                                <br />
                                {UPI_IMAGE}
                                <br />
                                <br />
                                <h6 style={{ display: 'inline-block'}}>{'UPI ID:'}&nbsp;&nbsp;</h6>
                                <span style={{ 
                                    fontWeight: "bold", 
                                    borderBottom: "2px solid red"}}
                                >
                                    {"flood-relief-helpify@okicici"}
                                </span>
                        </div>
                            <ReCAPTCHA 
                                sitekey={'6Lc-bbIUAAAAAMgF56DG4vB4juXTZZPVpyDm_MSn'}
                                onChange={this.onVerify} 
                                size={"compact"}
                            >
                            </ReCAPTCHA>
                    </ModalBody>
                    <ModalFooter>
                        <h6>Feel free to talk to us through the chat window below !</h6>
                    </ModalFooter>
                </Modal>
        </Card>
        )
    }
}