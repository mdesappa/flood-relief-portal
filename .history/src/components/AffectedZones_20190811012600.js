import { 
    Button,
    Card, 
    CardHeader, 
    CardBody,
    CardFooter,
  } from 'shards-react';

export default () => (
    <Card>
        <CardHeader className="hlp-card__header">
        <h3>⚠ Affected Areas</h3>
        <span>last updated 10 minutes ago</span>
        </CardHeader> 
        <CardBody style={{ maxHeight: 400, overflow: 'scroll'}}>
            {this.displayAffectedAreas()}
        </CardBody>
        <CardFooter>
        <Button>Add affected area</Button>
        </CardFooter>
    </Card>
)