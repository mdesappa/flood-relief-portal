import React from 'react';
import { Container, Row, Col } from 'shards-react';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="container">
         <div className="header">
              <h1>HELP PEOPLE RECOVER FROM THE FLOODS IN INDIA</h1>
         </div>
         <div className="content">
            <Container>
              <Row>
                    <Col sm="12" lg="4"  className="aff_areas">
                      <h2>Affected Areas</h2>
                    </Col>
                    <Col sm="12" lg="6" className="donate_area">
                      <h2>Donate Now</h2>
                    </Col>
              </Row>
              <Row>
                <Col lg="12" className="notification_area">
                  <h2>Send Notification To Others</h2>
                </Col>
              </Row>
            </Container>
            <Container className="dr-example-container">
        <Row>
          <Col sm="12" lg="6">
            sm=12 lg=6
          </Col>
          <Col sm="12" lg="6">
            sm=12 lg=6
          </Col>
        </Row>

        <Row>
          <Col sm="12" md="4" lg="3">
            sm=12 md=4 lg=3
          </Col>
          <Col sm="12" md="4" lg="6">
            sm=12 md=4 lg=6
          </Col>
          <Col sm="12" md="4" lg="3">
            sm=12 md=4 lg=3
          </Col>
        </Row>
        </Container>
         </div>
      </div>
    )
  }
}

export default App;
