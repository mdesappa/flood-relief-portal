import React from 'react';

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody,  \Slider } from 'shards-react';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <img src="./assets/helpify_logo.png"></img>
                  <span>Helpify For Flood Relief</span>
              </div>
              <div className="shoutout">
                  Help People Recover From Floods
              </div>
         </div>
         <div className="content">
            <Container>
              <Row>
                    <Col sm="12" lg="6"  className="aff_areas">
                      <Card>
                          <CardHeader>
                              <h2>Affected Areas</h2>
                          </CardHeader> 
                          <CardBody>
                              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                          </CardBody>
                      </Card>
                    </Col>
                    <Col sm="12" lg="6" className="donate_area">
                      <Card>
                            <CardHeader>
                                <h2>Donate Now</h2>
                            </CardHeader> 
                            <CardBody>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                            </CardBody>
                      </Card>
                    </Col>
              </Row>
              <Row>
                <Col sm="12" lg="6" className="notification_area">
                  <Card>
                      <CardHeader>
                            <h2>Affected Areas</h2>
                      </CardHeader> 
                      <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                      </CardBody>
                  </Card>
                </Col>
                <Col sm="12" lg="6" className="pickups_area">
                 <Card>
                    <CardHeader>
                        <h2>Donation Box Picup Areas</h2>
                    </CardHeader> 
                    <CardBody>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              <Row>
                  <Col sm="12" lg="6" className="emergency_contacts_area">
                    <Card>
                        <CardHeader>
                            <h2>Emergency Contact Area</h2>
                        </CardHeader> 
                        <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                        </CardBody>
                    </Card>
                  </Col>
              </Row>
            </Container>
         </div>
      </div>
    )
  }
}

export default App;
