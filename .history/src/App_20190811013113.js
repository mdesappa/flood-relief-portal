import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody,
} from 'shards-react';
import Img from 'react-image';

import './App.css';

import { DonationBox } from './components/DonationBox';
import { AffectedZones } from './components/AffectedZones';

class App extends React.Component {
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container fluid>
              <Row>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch">
                    <DonationBox />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch">
                    <AffectedZones />
                </Col>
              </Row>
          </Container>
          <Container fluid className="margin5">
            <Row>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch">
                  <Card>
                      <CardHeader>
                            <h3>Affected Areas</h3>
                      </CardHeader> 
                      <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                      </CardBody>
                  </Card>
                </Col>
                <Col sm="12" lg={{ size: 6  }} className="align-items-stretch">
                    <Card>
                        <CardHeader>
                            <h3>Donation Box Locations</h3>
                        </CardHeader> 
                        <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                        </CardBody>
                      </Card>
                </Col>
              </Row>
            </Container>
         </div>
      </div>
    )
  }
}

export default App;
