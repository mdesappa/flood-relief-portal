import React from 'react';

import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"

import { Container, Row, Col, Card, Slider } from 'shards-react';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <div>
         <div className="header">
              <h1>HELP PEOPLE RECOVER FROM THE FLOODS IN INDIA</h1>
         </div>
         <div className="content">
            <Container>
              <Row>
                    <Col sm="12" lg="6"  className="aff_areas">

                    </Col>
                    <Col sm="12" lg="6" className="donate_area">
                      <h2>Donate Now</h2>
                    </Col>
              </Row>
              <Row>
                <Col sm="12" lg="6" className="notification_area">
                  <h2>Send Notification To Others</h2>
                </Col>
                <Col sm="12" lg="6" className="pickups_area">
                  <h2>Clothes Pickup Location</h2>
                </Col>
              </Row>
              <Row>
                  <Col sm="12" lg="6" className="emergency_contacts_area">
                     <h2>Emergency Contacts</h2>
                  </Col>
              </Row>
            </Container>
         </div>
      </div>
    )
  }
}

export default App;
