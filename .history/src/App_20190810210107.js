import React from 'react';
import { Container, Row, Col } from 'shards-react';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="container">
         <div className="header">
              <h1>HELP PEOPLE RECOVER FROM THE FLOODS IN INDIA</h1>
         </div>
         <div className="content">
          <Row>
                  <Col className="aff_areas">
                    <h2>Affected Areas</h2>
                  </Col>
                  <Col>
                    <h2>Donate Now</h2>
                  </Col>
            </Row>
         </div>
      </div>
    )
  }
}

export default App;
