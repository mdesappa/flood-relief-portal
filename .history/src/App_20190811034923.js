import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardFooter,
  CardBody,
  Button,
} from 'shards-react';
import Img from 'react-image';
import GoogleMapReact from 'google-map-react';
import YouTube from 'react-youtube-embed';

import './App.css';

import { DonationBox } from './components/DonationBox';
import { AffectedZones } from './components/AffectedZones';
import { EmergencyContacts } from './components/EmergencyContacts';

const AnyReactComponent = ({ text }) => (<p>{text}</p>)

class App extends React.Component {
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container fluid>
              <Row>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <DonationBox />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <AffectedZones />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <EmergencyContacts />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <Card>
                        <CardHeader>
                            <h3>📺 Live News Feed</h3>
                        </CardHeader> 
                        <CardBody className="news_feed" style={{ minHeight: 500, maxHeight: 500, overflow: 'scroll' }}>
                          <YouTube id="arhjhHeuRUc"></YouTube>
                          <YouTube id="kpfV38Hcnds"></YouTube>
                        </CardBody>
                        <CardFooter>
                            <h6>👆Watch the above space for more updates</h6>
                        </CardFooter>
                      </Card>
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <Card>
                        <CardHeader>
                            <h3>Donation Boxes Near Me</h3>
                        </CardHeader> 
                        <CardBody style={{ maxHeight: 500, overflow: 'scroll', width: '100%', height: 500}}>
                            <GoogleMapReact
                              defaultCenter={{lat: 59.95, lng: 30.33}}
                              defaultZoom={11}
                            >
                                <AnyReactComponent 
                                  lat={59.955413} 
                                  lng={30.337844} 
                                  text={'Kreyser Avrora'} 
                                />
                            </GoogleMapReact>
                        </CardBody>
                        <CardFooter>
                            <Button outline theme={"danger"}> Add a new location </Button>
                        </CardFooter>
                      </Card>
                </Col>
              </Row>
          </Container>
         </div>
      </div>
    )
  }
}

export default App;
