import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody,
} from 'shards-react';
import Img from 'react-image';
import GoogleMapReact from 'google-map-react';
import YouTube from 'react-youtube-embed';

import './App.css';

import { DonationBox } from './components/DonationBox';
import { AffectedZones } from './components/AffectedZones';
import { EmergencyContacts } from './components/EmergencyContacts';

class App extends React.Component {
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container fluid>
              <Row>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <DonationBox />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <AffectedZones />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <EmergencyContacts />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <Card>
                        <CardHeader>
                            <h3>Donation Boxes Near Me</h3>
                        </CardHeader> 
                        <CardBody>
                            <div style={{  width: 'auto', overflow: 'scroll' }}>
                            <GoogleMapReact>
                            </GoogleMapReact>
                            </div>
                        </CardBody>
                      </Card>
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="align-items-stretch padding5">
                    <Card>
                        <CardHeader>
                            <h3>Live News Feed</h3>
                        </CardHeader> 
                        <CardBody>
                            <YouTube id="kpfV38Hcnds"></YouTube>
                        </CardBody>
                      </Card>
                </Col>
              </Row>
          </Container>
         </div>
      </div>
    )
  }
}

export default App;
