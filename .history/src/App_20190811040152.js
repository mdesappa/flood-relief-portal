import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardFooter,
  CardBody,
  Button,
  ButtonGroup,
} from 'shards-react';
import Img from 'react-image';
import GoogleMapReact from 'google-map-react';
import YouTube from 'react-youtube-embed';

import './App.css';

import { DonationBox } from './components/DonationBox';
import { AffectedZones } from './components/AffectedZones';
import { EmergencyContacts } from './components/EmergencyContacts';

const AnyReactComponent = ({ text }) => (<p>{text}</p>)

class App extends React.Component {
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container fluid>
              <Row>
                <Col sm="12" lg={{ size: 6 }} className="padding5">
                    <DonationBox />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="padding5">
                    <Card>
                        <CardHeader>
                            <h4>📺 Live News Feed</h4>
                        </CardHeader> 
                        <CardBody className="news_feed align-items-stretch">
                          <YouTube id="arhjhHeuRUc"></YouTube>
                          <YouTube id="kpfV38Hcnds"></YouTube>
                        </CardBody>
                        <CardFooter>
                            <h6>👆Watch the above space for more updates</h6>
                        </CardFooter>
                      </Card>
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="padding5">
                    <AffectedZones />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="padding5">
                    <EmergencyContacts />
                </Col>
                <Col sm="12" lg={{ size: 6 }} className="padding5">
                    <Card>
                        <CardHeader>
                            <h4>Donation Boxes Near Me</h4>
                        </CardHeader>
                        <CardBody className="card_height_fix">
                            
                             <div>
                              <ButtonGroup size="sm">
                                  <Button>Map</Button>
                                  <Button>List</Button>
                                </ButtonGroup>
                             </div>

                            <GoogleMapReact
                              defaultCenter={{lat: 59.95, lng: 30.33}}
                              defaultZoom={11}
                            >
                                <AnyReactComponent 
                                  lat={59.955413} 
                                  lng={30.337844} 
                                  text={'Kreyser Avrora'} 
                                />
                            </GoogleMapReact>
                        </CardBody>
                        <CardFooter>
                            <Button outline theme={"danger"}> Add a new location </Button>
                        </CardFooter>
                      </Card>
                </Col>
              </Row>
          </Container>
         </div>
      </div>
    )
  }
}

export default App;
