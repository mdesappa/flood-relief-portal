import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Button,
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody,
  CardFooter,
  CardTitle,
  ListGroup,
  ListGroupItem,
  Badge,
  Slider,
  FormGroup, 
} from 'shards-react';
import Img from 'react-image';

import './App.css';
import affected_areas from './data/affected_areas';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      donationValue: 50,
    };
    this.handleDonationValueChange = this.handleDonationValueChange.bind(this);
  }
  
  displayAffectedAreas() {
    const affected_areas_items = affected_areas.map(info => (
      <ListGroupItem>
          <span class="affected_areas--city_name">{info.city}</span> 
          <span class="affected_areas--state">{info.state}</span>
          <span class="affected_areas--severity_indicator">
            <Badge theme={info.severity}>Danger</Badge>
          </span>
        </ListGroupItem>
    ));
    return (
      <ListGroup>
        {affected_areas_items}
      </ListGroup>
    )
  }

  handleDonationValueChange(newValue) {
    this.setState({ donationValue: newValue });
  }
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container>
              <Row>
              <Col sm="12" lg="6" className="donation_area">
                      <Card>
                            <CardHeader>
                               {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                                <h3>💰Donate</h3>
                                <span>last donated 20 seconds ago</span>
                            </CardHeader> 
                            <CardBody>
                              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
                              <p className="donation_area--desc">
                                💚 You're precious donations could help save lives by providing 
                                <span className="donation_area--desc_item">clean water</span>
                                <span className="donation_area--desc_item">food</span>
                                <span className="donation_area--desc_item">snacks</span> 
                                to the afflicted zones.
                              </p>
                              <div>
                                  <h5 className="donation_value--text">I'd Like to Donate</h5>
                                  <span className="donation_value--rupee">₹{this.state.donationValue}</span>
                              </div>
                              <div className="center donation_value--slider">
                                <Slider
                                      style={{ marginTop: "-5px" }}
                                      pips={{ mode: "steps", stepped: true, density: 100 }}
                                      onSlide={this.handleDonationValueChange}
                                      start={this.state.donationValue}
                                      range={{ min: 50, max: 500 }}
                                      step={50}
                                  />
                              </div>
                              <div className="center donation_value--action">
                                  <Button outline block pill size="large">👉 Donate 👈</Button>
                              </div>
                            </CardBody>
                            <CardFooter>
                                <h4>We only accept UPI payments currently !</h4>
                            </CardFooter>
                      </Card>
                    </Col>
                   <Col sm="12" lg="6" className="affected_areas">
                      <Card>
                          <CardHeader className="hlp-card__header">
                            <h3>⚠ Affected Areas</h3>
                            <span>last updated 10 minutes ago</span>
                          </CardHeader> 
                          <CardBody style={{ maxHeight: 300,  overflowY: 'scroll' }}>
                              {this.displayAffectedAreas()}
                          </CardBody>
                          <CardFooter>
                            <Button>Add affected area</Button>
                          </CardFooter>
                      </Card>
                    </Col>
                    <Col sm="12" lg="6" className="notification_area">
                      <Card>
                          <CardHeader>
                                <h3>Affected Areas</h3>
                          </CardHeader> 
                          <CardBody>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                          </CardBody>
                      </Card>
                    </Col>
                  <Col sm="12" lg="6" className="pickups_area">
                      <Card>
                          <CardHeader>
                              <h3>Donation Box Locations</h3>
                          </CardHeader> 
                          <CardBody>
                              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                          </CardBody>
                        </Card>
                  </Col>
              </Row>
            </Container>
         </div>
      </div>
    )
  }
}

export default App;
