import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  CardHeader, 
  CardBody,
  ListGroup,
  ListGroupItem, 
  ListGroupItemHeading,
  Badge,
  Slider 
} from 'shards-react';
import Img from 'react-image';

import './App.css';
import affected_areas from './data/affected_areas';

class App extends React.Component {
  
  displayAffectedAreas() {
    const affected_areas_items = affected_areas.map(info => (
      <ListGroupItem>
          <span class="affected_areas--city_name">{info.city}</span> 
          <span class="affected_areas--state">{info.state}</span>
          <span class="affected_areas--severity_indicator">
            <Badge theme={info.severity}>Danger</Badge>
          </span>
        </ListGroupItem>
    ));
    return (
      <ListGroup>
        {affected_areas_items}
      </ListGroup>
    )
  }
  render() {
    return (
      <div className="container_root">
         <div className="header">
              <div className="logo_area">
                  <Img role="img" alt="helpify_logo" src="http://apphelpify.com/images/logo.png" height={40} width={40}></Img>
                  <span className="logo_text">Helpify</span>
              </div>
              {/* eslint-disable-next-line jsx-a11y/accessible-emoji  */}
              <div className="shoutout">
                  👋 Help People Recover From Floods in India 🙏
              </div>
         </div>
         <div className="content">
            <Container>
              <Row>
                    <Col sm="12" lg="6" className="affected_areas">
                      <Card>
                          <CardHeader>
                            <h2>Affected Areas</h2>
                          </CardHeader> 
                          <CardBody style={{ maxHeight: 300,  overflowY: 'scroll' }}>
                              {this.displayAffectedAreas()}
                          </CardBody>
                      </Card>
                    </Col>
                    <Col sm="12" lg="6" className="donate_area">
                      <Card>
                            <CardHeader>
                                <h2>Donate</h2>
                            </CardHeader> 
                            <CardBody>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                            </CardBody>
                      </Card>
                    </Col>
                    <Col sm="12" lg="6" className="notification_area">
                  <Card>
                      <CardHeader>
                            <h2>Affected Areas</h2>
                      </CardHeader> 
                      <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                      </CardBody>
                  </Card>
                </Col>
                <Col sm="12" lg="6" className="pickups_area">
                 <Card>
                    <CardHeader>
                        <h2>Donation Box Picup Areas</h2>
                    </CardHeader> 
                    <CardBody>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              <Row>
                  <Col sm="12" lg="6" className="emergency_contacts_area">
                    <Card>
                        <CardHeader>
                            <h2>Emergency Contact Area</h2>
                        </CardHeader> 
                        <CardBody>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore sapiente consectetur ab necessitatibus explicabo voluptates a sed rerum eaque autem? Inventore, quaerat. Quam veritatis unde harum ea vitae! Vero, in?
                        </CardBody>
                    </Card>
                  </Col>
              </Row>
            </Container>
         </div>
      </div>
    )
  }
}

export default App;
